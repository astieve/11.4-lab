﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        private static FileSystemWatcher watcher = new FileSystemWatcher(@"C:\Users\18sti\OneDrive - Northcentral Technical College\Courses\ADN\");

        private static string destinationFolder = @"C:\FileDestination\";

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog log = new EventLog();
            log.Source = "Application";
            log.WriteEntry("Service started");

            watcher.EnableRaisingEvents = true;

            watcher.Created += OnCreate;
            watcher.Changed += OnChange;
            watcher.Renamed += OnRename;
        }

        protected override void OnStop()
        {
            EventLog log = new EventLog();
            log.Source = "Application";
            log.WriteEntry("Service stopped");
        }

        private static void OnChange(object sender, FileSystemEventArgs e)
        {
            try
            {
                EventLog log = new EventLog();
                log.Source = "Application";
                log.WriteEntry($"File {e.FullPath} was moved.", EventLogEntryType.Information);
            }
            catch
            {
                EventLog errorLog = new EventLog();
                errorLog.Source = "Application";
                errorLog.WriteEntry("File was not moved.", EventLogEntryType.Error);
                Console.WriteLine("File could not be moved.");
            }
        }

        private static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, Path.Combine(destinationFolder, e.Name));

            }
            catch
            {
                EventLog errorLog = new EventLog();
                errorLog.Source = "Application";
                errorLog.WriteEntry("File was not moved.", EventLogEntryType.Error);
                Console.WriteLine("File could not be moved.");
            }
        }

        private static void OnRename(object sender, RenamedEventArgs e)
        {
            MoveFile(e.FullPath, Path.Combine(destinationFolder, e.Name));
        }

        private static void MoveFile(string sourcePath, string destinationPath)
        {
            if (File.Exists(sourcePath))
            {
                Thread.Sleep(1000);

                if (File.Exists(destinationPath))
                {
                    File.Delete(destinationPath);
                }

                File.Move(sourcePath, destinationPath);

                Console.WriteLine($"{sourcePath} moved to {destinationPath}");
            }
        }
    }
}
