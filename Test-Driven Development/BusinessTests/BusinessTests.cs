using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Test_Driven_Development;

namespace BusinessTests
{
    [TestClass]
    public class BusinessTests
    {
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void SecondJobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddJob(new Job(10));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[1].JobCompleted);
        }

        [TestMethod]
        public void PayGoesUpWhenDoingWork()
        {
            // Arrange
            Employee emp = new Employee(10, 20);
            Job job = new Job(10);

            // Act
            emp.DoWork(job);

            // Assert
            Assert.AreEqual(100, emp.Paycheck);
        }

        [TestMethod]
        public void JobCostGoesUpWhenDoingWork()
        {
            // Arrange
            Employee emp = new Employee(10, 20);
            Job job = new Job(10);

            // Act
            emp.DoWork(job);

            // Assert
            Assert.AreEqual(150, job.JobCost);
        }
    }
}
