﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Employee
    {
        private decimal hourlyWage;

        private int hoursScheduled;

        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        private Employee()
        { }

        public decimal Paycheck { get; set; }

        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.

            int hoursWorked = 0;

            if (hoursScheduled == 0)
            {
                // Do nothing.
            }
            else if (hoursScheduled < work.TimeInvestmentRemaining)
            {
                hoursWorked = hoursScheduled;

                work.TimeInvestmentRemaining -= hoursWorked;

                hoursScheduled = 0;
            }
            else
            {
                hoursWorked = work.TimeInvestmentRemaining;

                hoursScheduled -= work.TimeInvestmentRemaining;

                work.TimeInvestmentRemaining = 0;
            }

            decimal pay = this.hourlyWage * hoursWorked;

            this.Paycheck += pay;

            work.JobCost += pay * 1.5m;
        }
    }
}
