﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);

            Console.ReadLine();
        }

        public static void SerializeBusiness(Business business)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Business));

            using (Stream stream = File.Create(@"C:\Users\18sti\OneDrive - Northcentral Technical College\Courses\ADN\Biznazsty.xml"))
            {
                serializer.Serialize(stream, business);
            }
        }
    }
}
