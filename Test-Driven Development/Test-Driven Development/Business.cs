﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Business
    {
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        public List<Employee> Employees { get; set; }

        public List<Job> Jobs { get; set; }

        public void AddEmployee(Employee employee)
        {
            this.Employees.Add(employee);
        }

        public void AddJob(Job job)
        {
            this.Jobs.Add(job);
        }

        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
            foreach (Job j in this.Jobs)
            {
                if (j.TimeInvestmentRemaining > 0)
                {
                    foreach (Employee e in this.Employees)
                    {
                        e.DoWork(j);
                    }
                }
            }

        }
    }
}
